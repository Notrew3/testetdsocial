<?php
require_once('./config/config.php');

/**
 * 
 */
class Usuario
{

	private $table = 'usuarios';

	public function getData(){

		$conn = Connection::getInstance();
		$sql = "SELECT * FROM $this->table";
		$stm = $conn->prepare($sql);
		$stm->execute();
		$clientes = $stm->fetchAll(PDO::FETCH_ASSOC);

		return $clientes;

	}
}

?>